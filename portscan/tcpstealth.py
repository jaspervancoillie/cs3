#!/usr/bin/env python3
# (ip.src==192.168.211.128 && ip.dst==45.33.32.156) || (ip.dst==192.168.211.128 && ip.src==45.33.32.156)
from scapy.all import *
import os
import sys
sys.stderr = os.devnull  # suppress stderr
sys.stderr = sys.__stderr__  # restore stderr

dst_ip = "45.33.32.156"
src_port = 1025
dst_port = 80

try:
    stealth_scan_resp = sr1(IP(dst=dst_ip)/TCP(sport=src_port, dport=dst_port, flags="S", seq=1000), timeout=10, verbose=False)
    if stealth_scan_resp.getlayer(TCP).flags == "SA":
        sr1(IP(dst=dst_ip)/TCP(sport=src_port, dport=dst_port, flags="R", seq=stealth_scan_resp.seq + 1), timeout=10, verbose=False)
        print("port is listening")
    elif stealth_scan_resp.getlayer(TCP).flags == "RA" or stealth_scan_resp.getlayer(TCP).flags == "R":
        print("port is not listening")
    elif stealth_scan_resp.getlayer(ICMP).type == 3 and int(stealth_scan_resp.getlayer(ICMP).code in [1,2,3,9,10,13]):
        print("port filtered")
except:
    print("Something went wrong. Possibly the firewall did not return anything and the connection timed out")