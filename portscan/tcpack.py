#!/usr/bin/env python3
# (ip.src==192.168.211.128 && ip.dst==45.33.32.156) || (ip.dst==192.168.211.128 && ip.src==45.33.32.156)
from scapy.all import *
import os
import sys
sys.stderr = os.devnull  # suppress stderr
sys.stderr = sys.__stderr__  # restore stderr

dst_ip = "172.217.168.206"
src_port = 1025
dst_port = 100

try:
    ack_flag_scan_resp = sr1(IP(dst=dst_ip)/TCP(dport=dst_port,flags="A"),timeout=10, verbose=False)
    if ack_flag_scan_resp.getlayer(TCP).flags == "R" or ack_flag_scan_resp.getlayer(TCP).flags == "RA":
        print("Unfiltered")
    elif ack_flag_scan_resp.getlayer(ICMP).type == 3 and int(ack_flag_scan_resp.getlayer(ICMP).code in [1,2,3,9,10,13]):
        print("Filtered")
except:
    print("Something went wrong. Possibly the firewall did not return anything and the connection timed out")