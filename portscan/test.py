#!/usr/bin/env python3
#(ip.src==192.168.211.128 && ip.dst==45.33.32.156) || (ip.dst==192.168.211.128 && ip.src==45.33.32.156)
from scapy.all import *
import os
import sys

sys.stderr = os.devnull  # suppress stderr
sys.stderr = sys.__stderr__  # restore stderr

dst_ip = "45.33.32.156"
src_port = 1025
dst_port = 33
ports = [80, 443, 1234]

for i in ports:
    try:
        tcp_connect_scan_resp = sr1(IP(dst=dst_ip)/TCP(sport=src_port, dport=i, flags="S", seq=1000), timeout=10, verbose=False)   
        if tcp_connect_scan_resp.getlayer(TCP).flags == "SA":
            sr1(IP(dst=dst_ip)/TCP(sport=src_port, dport=i, flags="RA", seq=tcp_connect_scan_resp.ack+1, ack=tcp_connect_scan_resp.seq+1), timeout=10, verbose=False)
            print(str(i)+" - Open")
        elif tcp_connect_scan_resp.getlayer(TCP).flags == "R" or tcp_connect_scan_resp.getlayer(TCP).flags == "RA":
            print(str(i)+" - Closed")
    except:
        print(str(i)+" - Something went wrong. Possibly the firewall did not return anything and the connection timed out")