#!/usr/bin/env python3
# (ip.src==192.168.211.128 && ip.dst==45.33.32.156) || (ip.dst==192.168.211.128 && ip.src==45.33.32.156)
from scapy.all import *
import os
import sys
sys.stderr = os.devnull  # suppress stderr
sys.stderr = sys.__stderr__  # restore stderr

dst_ip = "192.168.88.101"
src_port = 1025
dst_port = 80

try:
    window_scan_resp = sr1(IP(dst=dst_ip)/TCP(dport=dst_port,flags="A"),timeout=10, verbose=False)
    if window_scan_resp.getlayer(TCP).window == 0:
        print("Closed")
    elif window_scan_resp.getlayer(TCP).window != 0:
        print("Open")
    elif window_scan_resp.getlayer(ICMP).type == 3 and int(window_scan_resp.getlayer(ICMP).code in [1,2,3,9,10,13]):
        print("port filtered")
except:
    print("Filtered")